<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220324105510 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE transcription (id INT AUTO_INCREMENT NOT NULL, filename VARCHAR(255) DEFAULT NULL, title VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE transcription_model_phrase_set (transcription_id INT NOT NULL, model_phrase_set_id INT NOT NULL, INDEX IDX_D386C126F678E194 (transcription_id), INDEX IDX_D386C1268BFA9A4C (model_phrase_set_id), PRIMARY KEY(transcription_id, model_phrase_set_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE transcription_model_phrase_set ADD CONSTRAINT FK_D386C126F678E194 FOREIGN KEY (transcription_id) REFERENCES transcription (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE transcription_model_phrase_set ADD CONSTRAINT FK_D386C1268BFA9A4C FOREIGN KEY (model_phrase_set_id) REFERENCES model_phrase_set (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE transcription_model_phrase_set DROP FOREIGN KEY FK_D386C126F678E194');
        $this->addSql('DROP TABLE transcription');
        $this->addSql('DROP TABLE transcription_model_phrase_set');
        $this->addSql('ALTER TABLE model_phrase CHANGE value value VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE model_phrase_set CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
