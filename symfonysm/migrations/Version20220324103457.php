<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220324103457 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE model_phrase (id INT AUTO_INCREMENT NOT NULL, value VARCHAR(255) NOT NULL, boost DOUBLE PRECISION DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE model_phrase_set (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, boost DOUBLE PRECISION DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE model_phrase_set_model_phrase (model_phrase_set_id INT NOT NULL, model_phrase_id INT NOT NULL, INDEX IDX_738246AD8BFA9A4C (model_phrase_set_id), INDEX IDX_738246ADBFB89917 (model_phrase_id), PRIMARY KEY(model_phrase_set_id, model_phrase_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE model_phrase_set_model_phrase ADD CONSTRAINT FK_738246AD8BFA9A4C FOREIGN KEY (model_phrase_set_id) REFERENCES model_phrase_set (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE model_phrase_set_model_phrase ADD CONSTRAINT FK_738246ADBFB89917 FOREIGN KEY (model_phrase_id) REFERENCES model_phrase (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE model_phrase_set_model_phrase DROP FOREIGN KEY FK_738246ADBFB89917');
        $this->addSql('ALTER TABLE model_phrase_set_model_phrase DROP FOREIGN KEY FK_738246AD8BFA9A4C');
        $this->addSql('DROP TABLE model_phrase');
        $this->addSql('DROP TABLE model_phrase_set');
        $this->addSql('DROP TABLE model_phrase_set_model_phrase');
    }
}
