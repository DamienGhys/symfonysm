<?php

namespace App\Controller;

use App\Entity\ModelPhrase;
use App\Form\ModelPhraseType;
use App\Repository\ModelPhraseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/model-phrase")
 */
class ModelPhraseController extends AbstractController
{
    /**
     * @Route("/", name="app_model_phrase_index", methods={"GET"})
     */
    public function index(ModelPhraseRepository $modelPhraseRepository): Response
    {
        return $this->render('model_phrase/index.html.twig', [
            'model_phrases' => $modelPhraseRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_model_phrase_new", methods={"GET", "POST"})
     */
    public function new(Request $request, ModelPhraseRepository $modelPhraseRepository): Response
    {
        $modelPhrase = new ModelPhrase();
        $form = $this->createForm(ModelPhraseType::class, $modelPhrase);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $modelPhraseRepository->add($modelPhrase);
            return $this->redirectToRoute('app_model_phrase_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('model_phrase/new.html.twig', [
            'model_phrase' => $modelPhrase,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_model_phrase_show", methods={"GET"})
     */
    public function show(ModelPhrase $modelPhrase): Response
    {
        return $this->render('model_phrase/show.html.twig', [
            'model_phrase' => $modelPhrase,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_model_phrase_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, ModelPhrase $modelPhrase, ModelPhraseRepository $modelPhraseRepository): Response
    {
        $form = $this->createForm(ModelPhraseType::class, $modelPhrase);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $modelPhraseRepository->add($modelPhrase);
            return $this->redirectToRoute('app_model_phrase_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('model_phrase/edit.html.twig', [
            'model_phrase' => $modelPhrase,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_model_phrase_delete", methods={"POST"})
     */
    public function delete(Request $request, ModelPhrase $modelPhrase, ModelPhraseRepository $modelPhraseRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$modelPhrase->getId(), $request->request->get('_token'))) {
            $modelPhraseRepository->remove($modelPhrase);
        }

        return $this->redirectToRoute('app_model_phrase_index', [], Response::HTTP_SEE_OTHER);
    }
}
