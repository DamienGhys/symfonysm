<?php

namespace App\Controller;

use App\Entity\Transcription;
use App\Form\TranscriptionType;
use App\Repository\TranscriptionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;


/**
 * @Route("/transcription")
 */
class TranscriptionController extends AbstractController
{
    /**
     * @Route("/", name="app_transcription_index", methods={"GET"})
     */
    public function index(TranscriptionRepository $transcriptionRepository): Response
    {
        return $this->render('transcription/index.html.twig', [
            'transcriptions' => $transcriptionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_transcription_new", methods={"GET", "POST"})
     */
    public function new(Request $request, TranscriptionRepository $transcriptionRepository): Response
    {
        $transcription = new Transcription();
        $form = $this->createForm(TranscriptionType::class, $transcription);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $transcriptionRepository->add($transcription);
            return $this->redirectToRoute('app_transcription_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('transcription/new.html.twig', [
            'transcription' => $transcription,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_transcription_show", methods={"GET"})
     */
    public function show(Transcription $transcription): Response
    {
        return $this->render('transcription/show.html.twig', [
            'transcription' => $transcription,
        ]);
    }

    /**
     * @Route("/json/{id}", name="app_transcription_show_json", methods={"GET"})
     */
    public function showJson(Transcription $transcription, SerializerInterface $serializer): Response
    {
        $jsonContent = $serializer->serialize($transcription, 'json');

        return $this->render('transcription/showJson.html.twig', [
            'json' => $jsonContent
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_transcription_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Transcription $transcription, TranscriptionRepository $transcriptionRepository): Response
    {
        $form = $this->createForm(TranscriptionType::class, $transcription);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $transcriptionRepository->add($transcription);
            return $this->redirectToRoute('app_transcription_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('transcription/edit.html.twig', [
            'transcription' => $transcription,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_transcription_delete", methods={"POST"})
     */
    public function delete(Request $request, Transcription $transcription, TranscriptionRepository $transcriptionRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$transcription->getId(), $request->request->get('_token'))) {
            $transcriptionRepository->remove($transcription);
        }

        return $this->redirectToRoute('app_transcription_index', [], Response::HTTP_SEE_OTHER);
    }
}
