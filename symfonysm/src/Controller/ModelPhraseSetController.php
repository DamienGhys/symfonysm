<?php

namespace App\Controller;

use App\Entity\ModelPhraseSet;
use App\Form\ModelPhraseSetType;
use App\Repository\ModelPhraseSetRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/model-phrase-set")
 */
class ModelPhraseSetController extends AbstractController
{
    /**
     * @Route("/", name="app_model_phrase_set_index", methods={"GET"})
     */
    public function index(ModelPhraseSetRepository $modelPhraseSetRepository): Response
    {
        return $this->render('model_phrase_set/index.html.twig', [
            'model_phrase_sets' => $modelPhraseSetRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_model_phrase_set_new", methods={"GET", "POST"})
     */
    public function new(Request $request, ModelPhraseSetRepository $modelPhraseSetRepository): Response
    {
        $modelPhraseSet = new ModelPhraseSet();
        $form = $this->createForm(ModelPhraseSetType::class, $modelPhraseSet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $modelPhraseSetRepository->add($modelPhraseSet);
            return $this->redirectToRoute('app_model_phrase_set_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('model_phrase_set/new.html.twig', [
            'model_phrase_set' => $modelPhraseSet,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_model_phrase_set_show", methods={"GET"})
     */
    public function show(ModelPhraseSet $modelPhraseSet): Response
    {
        return $this->render('model_phrase_set/show.html.twig', [
            'model_phrase_set' => $modelPhraseSet,
        ]);
    }

    /**
     * @Route("/json/{id}", name="app_model_phrase_set_show_json", methods={"GET"})
     */
    public function showJson(ModelPhraseSet $modelPhraseSet, SerializerInterface $serializer): Response
    {
        $jsonContent = $serializer->serialize($modelPhraseSet, 'json');

        return $this->render('model_phrase_set/showJson.html.twig', [
            'json' => $jsonContent
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_model_phrase_set_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, ModelPhraseSet $modelPhraseSet, ModelPhraseSetRepository $modelPhraseSetRepository): Response
    {
        $form = $this->createForm(ModelPhraseSetType::class, $modelPhraseSet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $modelPhraseSetRepository->add($modelPhraseSet);
            return $this->redirectToRoute('app_model_phrase_set_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('model_phrase_set/edit.html.twig', [
            'model_phrase_set' => $modelPhraseSet,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_model_phrase_set_delete", methods={"POST"})
     */
    public function delete(Request $request, ModelPhraseSet $modelPhraseSet, ModelPhraseSetRepository $modelPhraseSetRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$modelPhraseSet->getId(), $request->request->get('_token'))) {
            $modelPhraseSetRepository->remove($modelPhraseSet);
        }

        return $this->redirectToRoute('app_model_phrase_set_index', [], Response::HTTP_SEE_OTHER);
    }
}
