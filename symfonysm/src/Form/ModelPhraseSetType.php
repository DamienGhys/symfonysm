<?php

namespace App\Form;

use App\Entity\ModelPhrase;
use App\Entity\ModelPhraseSet;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ModelPhraseSetType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('boost')
            ->add('modelPhrases', EntityType::class, [
                'class' => ModelPhrase::class,
                'multiple' => true,
                'expanded' => true,
                'choice_label' => 'value',
            ])
            //->add('transcriptions')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ModelPhraseSet::class,
        ]);
    }
}
