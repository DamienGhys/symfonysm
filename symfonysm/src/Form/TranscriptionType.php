<?php

namespace App\Form;

use App\Entity\Transcription;
use App\Entity\ModelPhraseSet;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TranscriptionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('filename')
            ->add('title')
            ->add('phraseSets', EntityType::class, [
                'class' => ModelPhraseSet::class,
                'multiple' => true,
                'expanded' => true,
                'choice_label' => 'name'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Transcription::class,
        ]);
    }
}
