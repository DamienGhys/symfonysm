<?php

namespace App\Entity;

use Symfony\Component\Serializer\Annotation\Groups;
use App\Repository\ModelPhraseSetRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Ignore;

/**
 * @ORM\Entity(repositoryClass=ModelPhraseSetRepository::class)
 */
class ModelPhraseSet
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $boost;

    /**
     * @ORM\ManyToMany(targetEntity=ModelPhrase::class, inversedBy="modelPhraseSets")
     */
    private $modelPhrases;

    /**
     * @ORM\ManyToMany(targetEntity=Transcription::class, mappedBy="PhraseSets")
     * @Ignore()
     */
    private $transcriptions;

    public function __construct()
    {
        $this->modelPhrases = new ArrayCollection();
        $this->transcriptions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBoost(): ?float
    {
        return $this->boost;
    }

    public function setBoost(?float $boost): self
    {
        $this->boost = $boost;

        return $this;
    }

    /**
     * @return Collection<int, ModelPhrase>
     */
    public function getModelPhrases(): Collection
    {
        return $this->modelPhrases;
    }

    public function addModelPhrase(ModelPhrase $modelPhrase): self
    {
        if (!$this->modelPhrases->contains($modelPhrase)) {
            $this->modelPhrases[] = $modelPhrase;
        }

        return $this;
    }

    public function removeModelPhrase(ModelPhrase $modelPhrase): self
    {
        $this->modelPhrases->removeElement($modelPhrase);

        return $this;
    }

    /**
     * @return Collection<int, Transcription>
     */
    public function getTranscriptions(): Collection
    {
        return $this->transcriptions;
    }

    public function addTranscription(Transcription $transcription): self
    {
        if (!$this->transcriptions->contains($transcription)) {
            $this->transcriptions[] = $transcription;
            $transcription->addPhraseSet($this);
        }

        return $this;
    }

    public function removeTranscription(Transcription $transcription): self
    {
        if ($this->transcriptions->removeElement($transcription)) {
            $transcription->removePhraseSet($this);
        }

        return $this;
    }
}
