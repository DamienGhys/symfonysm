<?php

namespace App\Entity;

use Symfony\Component\Serializer\Annotation\Groups;
use App\Repository\ModelPhraseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Ignore;

/**
 * @ORM\Entity(repositoryClass=ModelPhraseRepository::class)
 */
class ModelPhrase
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $value;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $boost;

    /**
     * @ORM\ManyToMany(targetEntity=ModelPhraseSet::class, mappedBy="modelPhrases")
     * @Ignore()
     */
    private $modelPhraseSets;

    public function __construct()
    {
        $this->modelPhraseSets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getBoost(): ?float
    {
        return $this->boost;
    }

    public function setBoost(?float $boost): self
    {
        $this->boost = $boost;

        return $this;
    }

    /**
     * @return Collection<int, ModelPhraseSet>
     */
    public function getModelPhraseSets(): Collection
    {
        return $this->modelPhraseSets;
    }

    public function addModelPhraseSet(ModelPhraseSet $modelPhraseSet): self
    {
        if (!$this->modelPhraseSets->contains($modelPhraseSet)) {
            $this->modelPhraseSets[] = $modelPhraseSet;
            $modelPhraseSet->addModelPhrase($this);
        }

        return $this;
    }

    public function removeModelPhraseSet(ModelPhraseSet $modelPhraseSet): self
    {
        if ($this->modelPhraseSets->removeElement($modelPhraseSet)) {
            $modelPhraseSet->removeModelPhrase($this);
        }

        return $this;
    }
}
