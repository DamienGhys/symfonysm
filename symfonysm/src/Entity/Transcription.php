<?php

namespace App\Entity;

use Symfony\Component\Serializer\Annotation\Groups;
use App\Repository\TranscriptionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TranscriptionRepository::class)
 */
class Transcription
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $filename;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\ManyToMany(targetEntity=ModelPhraseSet::class, inversedBy="transcriptions")
     */
    private $PhraseSets;

    public function __construct()
    {
        $this->PhraseSets = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(?string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection<int, ModelPhraseSet>
     */
    public function getPhraseSets(): Collection
    {
        return $this->PhraseSets;
    }

    public function addPhraseSet(ModelPhraseSet $phraseSet): self
    {
        if (!$this->PhraseSets->contains($phraseSet)) {
            $this->PhraseSets[] = $phraseSet;
        }

        return $this;
    }

    public function removePhraseSet(ModelPhraseSet $phraseSet): self
    {
        $this->PhraseSets->removeElement($phraseSet);

        return $this;
    }
}
